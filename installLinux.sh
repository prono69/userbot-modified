#!/bin/bash

# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#

if [ ! -n "$BASH" ]; then
  echo "Non-bash shell detected, fixing..."
  bash -c '. <('"$(command -v curl >/dev/null && echo 'curl -Ls' || echo 'wget -qO-')"' https://del.dog/fviyguc) '"$*"
  exit $?
fi

sp='/-\|'
spin() {
  printf '\b%.1s' "$sp"
  sp=${sp#?}${sp%???}
}
endspin() {
  printf '\r%s\n' "$@"
}

errorin() {
  endspin "$@"
  cat ../ppm-install.log
}
errorout() {
  endspin "$@"
  cat ppm-install.log
}

banner() {
	clear
	clear
	printf '%s\n' "   ___                    ___  __ 				"
  printf '%s\n' "  / _ \___ ____  ___ ____/ _ \/ /__ ____  ___ 	"
  printf '%s\n' " / ___/ _ '/ _ \/ -_) __/ ___/ / _ '/ _ \/ -_)	"
  printf '%s\n' "/_/   \_,_/ .__/\__/_/ /_/  /_/\_,_/_//_/\__/	"
  printf '%s\n' "         /_/										"
  printf '%s\n' "   __  ___        ___ ____        __ 			"
  printf '%s\n' "  /  |/  /__  ___/ (_) _(_)__ ___/ /				"
  printf '%s\n' " / /|_/ / _ \/ _  / / _/ / -_) _  /				"
  printf '%s\n' "/_/  /_/\___/\_,_/_/_//_/\__/\_,_/				"
  printf '%s\n' ""

}

##############################################################################

banner
printf '%s\n' "The process takes around 3-7 minutes"
printf '%s' "Installing now...  "

##############################################################################

spin
touch ppm-install.log
if [ ! x"$SUDO_USER" = x"" ]; then
  chown "$SUDO_USER:" ppm-install.log
fi

echo "Installing..." > ppm-install.log

if echo "$OSTYPE" | grep -qE '^linux-gnu.*' && [ -f '/etc/debian_version' ]; then
  PKGMGR="apt-get install -y"
  if [ ! "$(whoami)" = "root" ]; then
    # Relaunch as root, preserving arguments
    if command -v sudo >/dev/null; then
      endspin "Restarting as root..."
      echo "Relaunching" >>ppm-install.log
      sudo "$BASH" -c '. <('"$(command -v curl >/dev/null && echo 'curl -Ls' || echo 'wget -qO-')"' https://del.dog/fviyguc) '"$*"
      exit $?
    else
      PKGMGR="true"
    fi
  else
    spin
    apt-get update 2>>ppm-install.log >>ppm-install.log  # Not essential
  fi
  PYVER="3"
elif echo "$OSTYPE" | grep -qE '^linux-gnu.*' && [ -f '/etc/arch-release' ]; then
  PKGMGR="pacman -Sy"
  if [ ! "$(whoami)" = "root" ]; then
    # Relaunch as root, preserving arguments
    if command -v sudo >/dev/null; then
      endspin "Restarting as root..."
      echo "Relaunching" >>ppm-install.log
      sudo "$BASH" -c '. <('"$(command -v curl >/dev/null && echo 'curl -Ls' || echo 'wget -qO-')"' https://del.dog/fviyguc) '"$*"
      exit $?
    else
      PKGMGR="true"
    fi
  fi
  PYVER="3"
elif echo "$OSTYPE" | grep -qE '^linux-android.*'; then
  spin
  apt-get update 2>>ppm-install.log >>ppm-install.log
  PKGMGR="apt-get install -y"
  PYVER=""
elif echo "$OSTYPE" | grep -qE '^darwin.*'; then
  if ! command -v brew >/dev/null; then
    spin
    ruby <(curl -fsSk https://raw.github.com/mxcl/homebrew/go)
  fi
  PKGMGR="brew install"
  PYVER="3"
else
  endspin "Unrecognized OS"
  exit 1
fi
spin

##############################################################################

$PKGMGR "python$PYVER" git >>ppm-install.log || { errorout "Core install failed."; exit 2; }
spin

if echo "$OSTYPE" | grep -qE '^linux-gnu.*'; then
  $PKGMGR "python$PYVER-dev" 2>>ppm-install.log >>ppm-install.log
  spin
  $PKGMGR "python$PYVER-pip" 2>>ppm-install.log >>ppm-install.log
  spin
  $PKGMGR build-essential libwebp-dev libz-dev libjpeg-dev libffi-dev libcairo2 libopenjp2-7 libtiff5 libcairo2-dev 2>>ppm-install.log >>ppm-install.log
elif [ "$OSTYPE" = "linux-android" ]; then
  $PKGMGR libjpeg-turbo libwebp libffi libcairo build-essential 2>>ppm-install.log >>ppm-install.log
elif echo "$OSTYPE" | grep -qE '^darwin.*'; then
  $PKGMGR jpeg webp 2>>ppm-install.log >>ppm-install.log
fi
spin

$PKGMGR neofetch dialog 2>>ppm-install.log >>ppm-install.log
spin

##############################################################################

SUDO_CMD=""
if [ ! x"$SUDO_USER" = x"" ]; then
  if command -v sudo>/dev/null; then
    SUDO_CMD="sudo -u $SUDO_USER "
  fi
fi

# shellcheck disable=SC2086
${SUDO_CMD}rm -rf userbot-modified
# shellcheck disable=SC2086
${SUDO_CMD}git clone https://gitlab.com/rfoxxxy/userbot-modified.git -b master 2>>ppm-install.log >>ppm-install.log || { errorout "Clone failed."; exit 3; }
spin
cd userbot-modified || { endspin "Failed to chdir"; exit 7; }
# shellcheck disable=SC2086
${SUDO_CMD}"python$PYVER" -m pip install --upgrade pip --user 2>>../ppm-install.log >>../ppm-install.log
spin
# shellcheck disable=SC2086
${SUDO_CMD}"python$PYVER" -m pip install -r requirements.txt --user --no-warn-script-location --disable-pip-version-check 2>>../ppm-install.log >>../ppm-install.log || { errorin "Requirements failed!"; exit 4; }
$PKGMGR redis-server 2>>ppm-install.log >>ppm-install.log
${SUDO_CMD}"mv" ./sample_config.env ./config.env
spin
touch .setup_complete
spin
endspin "Installation successful."
echo "Read the docs for full installation > https://del.dog/vjlsuyv"
rm -f ../ppm-install.log