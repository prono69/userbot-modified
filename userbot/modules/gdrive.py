# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#
# The entire source code is OSSRPL except
# 'download, uploadir, uploadas, upload' which is MPL
# License: MPL and OSSRPL
#
# modified by rfoxxxy (rfoxxxyz studio dev.)
# @rfoxxxyofficial
""" Userbot module which contains everything related to
    downloading/uploading from/to the server. """

import json
import logging
import mimetypes
import os
import re
import subprocess
from datetime import datetime
from io import BytesIO
from time import sleep

import psutil
from hachoir.metadata import extractMetadata
from hachoir.parser import createParser
from pyDownload import Downloader
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from telethon.tl.types import DocumentAttributeVideo, MessageMediaPhoto

from userbot import CMD_HELP, GDRIVE_FOLDER, LOGS
from userbot.events import register

TEMP_DOWNLOAD_DIRECTORY = os.environ.get("TMP_DOWNLOAD_DIRECTORY", "./")


def progress(current, total):
    """ Logs the download progress """
    LOGS.info("Downloaded %s of %s\nCompleted %s", current, total,
              (current / total) * 100)


async def download_from_url(url: str, file_name: str) -> str:
    """
    Download files from URL
    """
    start = datetime.now()
    downloader = Downloader(url=url)
    if downloader.is_running:
        sleep(1)
    end = datetime.now()
    duration = (end - start).seconds
    os.rename(downloader.file_name, file_name)
    status = f"Downloaded `{file_name}` in {duration} seconds."
    return status


async def download_from_tg(target_file) -> (str, BytesIO):
    """
    Download files from Telegram
    """
    async def dl_file(buffer: BytesIO) -> BytesIO:
        buffer = await target_file.client.download_media(
            reply_msg,
            buffer,
            progress_callback=progress,
        )
        return buffer

    start = datetime.now()
    buf = BytesIO()
    reply_msg = await target_file.get_reply_message()
    avail_mem = psutil.virtual_memory().available + psutil.swap_memory().free
    try:
        if reply_msg.media.document.size >= avail_mem:  # unlikely to happen but baalaji crai
            filen = await target_file.client.download_media(
                reply_msg,
                progress_callback=progress,
            )
        else:
            buf = await dl_file(buf)
            filen = reply_msg.media.document.attributes[0].file_name
    except AttributeError:
        buf = await dl_file(buf)
        try:
            filen = reply_msg.media.document.attributes[0].file_name
        except AttributeError:
            if isinstance(reply_msg.media, MessageMediaPhoto):
                filen = 'photo-' + str(datetime.today())\
                    .split('.')[0].replace(' ', '-') + '.jpg'
            else:
                filen = reply_msg.media.document.mime_type\
                    .replace('/', '-' + str(datetime.today())
                             .split('.')[0].replace(' ', '-') + '.')
    end = datetime.now()
    duration = (end - start).seconds
    await target_file.edit(f"`Downloaded {filen} in {duration} seconds.`")
    return filen, buf


async def gdrive_upload(filename: str, filebuf: BytesIO = None) -> str:
    """
    Upload files to Google Drive using PyDrive
    """
    # a workaround for disabling cache errors
    # https://github.com/googleapis/google-api-python-client/issues/299
    logging.getLogger('googleapiclient.discovery_cache').setLevel(
        logging.CRITICAL)

    # Authenticate Google Drive automatically
    # https://stackoverflow.com/a/24542604
    gauth = GoogleAuth()
    # Try to load saved client credentials
    gauth.LoadCredentialsFile("secret.json")
    if gauth.credentials is None:
        return "nosecret"
    if gauth.access_token_expired:
        gauth.Refresh()
    else:
        # Initialize the saved credentials
        gauth.Authorize()
    # Save the current credentials to a file
    gauth.SaveCredentialsFile("secret.json")
    drive = GoogleDrive(gauth)

    if filename.count('/') > 1:
        filename = filename.split('/')[-1]
    filedata = {
        'title': filename,
        "parents": [{
            "kind": "drive#fileLink",
            "id": GDRIVE_FOLDER
        }]
    }

    if filebuf:
        mime_type = mimetypes.guess_type(filename)
        if mime_type[0] and mime_type[1]:
            filedata['mimeType'] = f"{mime_type[0]}/{mime_type[1]}"
        else:
            filedata['mimeType'] = 'text/plain'
        file = drive.CreateFile(filedata)
        file.content = filebuf
    else:
        file = drive.CreateFile(filedata)
        file.SetContentFile(filename)
    name = filename.split('/')[-1]
    file.Upload()
    # insert new permission
    file.InsertPermission({
        'type': 'anyone',
        'value': 'anyone',
        'role': 'reader'
    })
    if not filebuf:
        os.remove(filename)
    reply = f"[{name}]({file['alternateLink']})\n" \
        f"__Direct link:__ [Here]({file['downloadUrl']})"
    return reply


@register(pattern=r"^.mirror(?: |$)([\s\S]*)", outgoing=True)
async def gdrive_mirror(request):
    """ Download a file and upload to Google Drive """
    message = request.pattern_match.group(1)
    if not request.reply_to_msg_id and not message:
        await request.edit("`Usage: .mirror <url> <url>`")
        return
    reply = ''
    reply_msg = await request.get_reply_message()
    links = re.findall(r'\bhttps?://.*\.\S+', message)
    if not (links or reply_msg or reply_msg.media or reply_msg.media.document):
        reply = "`No links or telegram files found!`\n"
        await request.edit(reply)
        return
    if request.reply_to_msg_id:
        await request.edit('`Downloading from Telegram...`')
        filen, buf = await download_from_tg(request)
        await request.edit(f'`Uploading {filen} to GDrive...`')
        reply += await gdrive_upload(
            filen, buf) if buf else await gdrive_upload(filen)
    elif "|" in message:
        url, file_name = message.split("|")
        url = url.strip()
        file_name = file_name.strip()
        await request.edit(f'`Downloading {file_name}`')
        status = await download_from_url(url, file_name)
        await request.edit(status)
        await request.edit(f'`Uploading {file_name} to GDrive...`')
        reply += await gdrive_upload(file_name)
    if "nosecret" in reply:
        reply = "`Run the generate_drive_session.py file " \
                "in your machine to authenticate on google drive!!`"
        await request.edit(reply)


@register(pattern=r"^.drive(?: |$)(\S*.?\/*.?\.?[A-Za-z0-9]*)", outgoing=True)
async def gdrive(request):
    """ Upload files from server to Google Drive """
    path = request.pattern_match.group(1)
    if not path:
        await request.edit("`Usage: .drive <file>`")
        return
    if not os.path.isfile(path):
        await request.edit("`No such file!`\n")
        return
    file_name = path.split('/')[-1]
    await request.edit(f'`Uploading {file_name} to GDrive...`')
    reply = await gdrive_upload(path)
    await request.edit(reply)

def get_video_thumb(file, output=None, width=90):
    """ Get video thhumbnail """
    metadata = extractMetadata(createParser(file))
    popen = subprocess.Popen(
        [
            "ffmpeg",
            "-i",
            file,
            "-ss",
            str(
                int((0, metadata.get("duration").seconds
                     )[metadata.has("duration")] / 2)),
            "-filter:v",
            "scale={}:-1".format(width),
            "-vframes",
            "1",
            output,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
    )
    if not popen.returncode and os.path.lexists(file):
        return output
    return None


def extract_w_h(file):
    """ Get width and height of media """
    command_to_run = [
        "ffprobe",
        "-v",
        "quiet",
        "-print_format",
        "json",
        "-show_format",
        "-show_streams",
        file,
    ]
    # https://stackoverflow.com/a/11236144/4723940
    try:
        t_response = subprocess.check_output(command_to_run,
                                             stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as exc:
        LOGS.warning(exc)
    else:
        x_reponse = t_response.decode("UTF-8")
        response_json = json.loads(x_reponse)
        width = int(response_json["streams"][0]["width"])
        height = int(response_json["streams"][0]["height"])
        return width, height

CMD_HELP.update({
    "drive":
    ".upload <file>\nUsage: Upload a locally stored file to GDrive."
})
CMD_HELP.update({
    "mirror":
    ".mirror [in reply to TG file]\n"
    "or .mirror <link> | <filename>\n"
    "Usage: Download a file from telegram "
    "or link to the server then upload to your GDrive."
})