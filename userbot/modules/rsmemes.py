# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#
#

"""rsMeme module

this module contains kanged features from another userbot's ;)

"""

from userbot import CMD_HELP, LOGS
from userbot.events import register
import io
import requests
import os
from telethon.tl.types import DocumentAttributeFilename, MessageMediaPhoto
from io import BytesIO

TEMP_DOWNLOAD_DIRECTORY = os.environ.get("TEMP_DOWNLOAD_DIRECTORY", "./downloads")

"""RemoveBG Staff"""
try:
	from userbot import REM_BG_API_KEY
except Exception:
	REM_BG_API_KEY = os.environ.get("REM_BG_API_KEY", None)

"""Barcode staff"""
import barcode
import time
from barcode.writer import ImageWriter
from datetime import datetime

"""Genius(lyrics) staff"""
try:
	from userbot import GENIUS_API_TOKEN as GApi
except Exception:
	GApi = os.environ.get("GENIUS_API_TOKEN", 'None')
import lyricsgenius
genius = lyricsgenius.Genius(GApi)

"""Figlet staff"""
import random
from pyfiglet import Figlet, FigletFont, FontNotFound

"""Gifify staff"""
import json
import asyncio
import functools
try:
	import tgs
except OSError:
	pass
from PIL import Image
def run_sync(func, *args, **kwargs):
	# Returning a coro
	return asyncio.get_event_loop().run_in_executor(None, functools.partial(func, *args, **kwargs))

async def answer(message, answer, **kwargs):
	CONT_MSG = "[continued]\n"
	ret = [message]
	if isinstance(answer, str) and not kwargs.get("asfile", False):
		await message.edit(answer)
		answer = answer[4096:]
		while len(answer) > 0:
			answer = CONT_MSG + answer
			message.message = answer[:4096]
			answer = answer[4096:]
			ret.append(await message.respond(message, **kwargs))
	else:
		if message.media is not None:
			await message.edit(file=answer, **kwargs)
		else:
			await message.edit("`Loading media...`")
			ret = [await message.client.send_file(message.to_id, answer, reply_to=message.reply_to_msg_id, **kwargs)]
			await message.delete()

"""Moon and clock staff"""
from collections import deque

"""Google staff"""
from bs4 import BeautifulSoup
from google_images_download import google_images_download

def progress(current, total):
	LOGS.info("Downloaded %s of %s\nCompleted %s", current, total, (current / total) * 100)

"""----------------"""

@register(outgoing=True, pattern='^.figlet(?: |$)(.*)')
async def figl(fag):
	await fag.edit("`Generating figlet...`")
	args = fag.text.split()

	if len(args) < 3:
		await fag.edit('`Provide a font and some text to render it with figlet!`')
		return

	font = args[1]
	ftextinit = fag.text.split(' ', 2)
	ftext = ftextinit[2]

	if font == 'random':
		font = random.choice(FigletFont.getFonts())

	try:
		fig = Figlet(font=font, width=30)
	except FontNotFound:
		await fag.edit(f"`Font {font} not found`")
		return

	await fag.edit("```\u206a" + fig.renderText(ftext) + "```")

@register(outgoing=True, pattern='^.gifify(?: |$)(.*)')
async def gifify(geef):
		args = geef.text.split()
		fps = 5
		quality = 256
		try:
			if len(args) == 2:
				fps = int(args[1])
			elif len(args) == 3:
				quality = int(args[1])
				fps = int(args[2])
		except Exception:
			geef.edit("`Failed to parse quality/fps`")
		target = await geef.get_reply_message()
		if target is None or target.file is None or target.file.mime_type != "application/x-tgsticker":
			await geef.edit("`Please provide an animated sticker to convert to a GIF`")
			return
		try:
			file = BytesIO()
			await geef.client.download_media(file)
			file.seek(0)
			anim = await run_sync(tgs.parsers.tgs.parse_tgs, file)
			file.close()
			result = BytesIO()
			result.name = "animation.gif"
			await run_sync(tgs.exporters.gif.export_gif, anim, result, quality, fps)
			result.seek(0)
			await answer(geef, result)
		finally:
			try:
				file.close()
			except UnboundLocalError:
				pass
			try:
				result.close()
			except UnboundLocalError:
				pass

@register(outgoing=True, pattern='^.lyrics(?: |$)(.*)')
async def lyrics(lyr):
	if GApi == 'None':
		await lyr.edit(
			"`Kek provide genius api token to env or config.env first kthx!`"
		)
	try:
		args = lyr.text.split()
		artist = lyr.text.split()[1]
		snameinit = lyr.text.split(' ', 2)
		sname = snameinit[2]
	except Exception:
		await lyr.edit("`Lel pls provide artist and song names`")
		return

	#Try to search for * in artist string(for multiword artist name)
	try:
		artist = artist.replace('*', ' ')
	except Exception:
		artist = lyr.text.split()[1]
		pass

	if len(args) < 3:
		await lyr.edit("`Please provide artist and song names`")

	await lyr.edit(f"`Searching lyrics for {artist} - {sname}...`")

	try:
		song = genius.search_song(sname, artist)
	except TypeError:
		song = None

	if song is None:
		await lyr.edit(f"Song **{artist} - {sname}** not found!")
		return
	if len(song.lyrics) > 4096:
			await lyr.edit("`Lyrics is too big, view the file to see it.`")
			file = open("lyrics.txt", "w+")
			file.write(f"Search query: \n{artist} - {sname}\n\n{song.lyrics}")
			file.close()
			await lyr.client.send_file(
				lyr.chat_id,
				"lyrics.txt",
				reply_to=lyr.id,
			)
			os.remove("lyrics.txt")
	else:
		await lyr.edit(f"**Search query**: \n`{artist} - {sname}`\n\n```{song.lyrics}```")
	return

@register(outgoing=True, pattern='^.barcode(?: |$)(.*)')
async def barcode(bc):
	if bc.fwd_from:
		return
	await bc.edit("`Obtaining barcode...`")
	start = datetime.now()
	input_str = bc.pattern_match.group(1)
	reply_msg_id = bc.message.id
	if input_str:
		message = input_str
	elif bc.reply_to_msg_id:
		previous_message = await bc.get_reply_message()
		reply_msg_id = previous_message.id
		if previous_message.media:
			downloaded_file_name = await bc.client.download_media(
				previous_message,
				TEMP_DOWNLOAD_DIRECTORY,
			)
			m_list = None
			with open(downloaded_file_name, "rb") as fd:
				m_list = fd.readlines()
			message = ""
			for m in m_list:
				message += m.decode("UTF-8") + "\r\n"
			os.remove(downloaded_file_name)
		else:
			message = previous_message.message
	else:
		message = "`Hmmmm you're forgot to provide a string for bcodeeeng`"
	bar_code_type = "code128"
	try:
		bar_code_mode_f = barcode.get(bar_code_type, message, writer=ImageWriter())
		filename = bar_code_mode_f.save(bar_code_type)
		await bc.client.send_file(
			bc.chat_id,
			filename,
			caption=message,
			reply_to=reply_msg_id,
		)
		os.remove(filename)
	except Exception as e:
		await bc.edit(str(e))
		return
	end = datetime.now()
	ms = (end - start).seconds
	await bc.edit("Obtained BarCode in {} seconds".format(ms))
	await asyncio.sleep(5)
	bc.delete()

@register(outgoing=True, pattern='^.(coinflip|coenflip|coinflep|coen|coin|cf)$')
async def coen(cf):
	if cf.fwd_from:
		return
	r = random.randint(1, 101)
	if r <= 50:
		msg = 'The coin landed on: **Heads**.'
	elif r >= 50 and r < 101:
		msg = 'The coin landed on: **Tails**.'
	else:
		msg = 'Yay u\'re lucky man. The coin landed on: **Side**'

	await cf.edit(msg)

@register(outgoing=True, pattern='^.moon$')
async def moon(mn):
	if mn.fwd_from:
		return
	deq = deque(list("🌗🌘🌑🌒🌓🌔🌕🌖"))
	for _ in range(32):
		await asyncio.sleep(0.1)
		await mn.edit("".join(deq))
		deq.rotate(1)

@register(outgoing=True, pattern='^.clock$')
async def clock(ck):
	if ck.fwd_from:
		return
	deq = deque(list("🕙🕘🕗🕖🕕🕔🕓🕒🕑🕐🕛"))
	for _ in range(48):
		await asyncio.sleep(0.1)
		await ck.edit("".join(deq))
		deq.rotate(1)

@register(outgoing=True, pattern='^.gs(?: |$)(.*)')
async def gsearch(gs):
	if gs.fwd_from:
		return
	start = datetime.now()
	await gs.edit("`🔎 Trying the google magic ...`")
	# SHOW_DESCRIPTION = False
	input_str = gs.pattern_match.group(1) # + " -inurl:(htm|html|php|pls|txt) intitle:index.of \"last modified\" (mkv|mp4|avi|epub|pdf|mp3)
	input_url = "https://bots.shrimadhavuk.me/search/?q={}".format(input_str)
	headers = {"USER-AGENT": "UniBorg"}
	response = requests.get(input_url, headers=headers).json()
	output_str = " "
	for result in response["results"]:
		text = result.get("title")
		url = result.get("url")
		description = result.get("description")
		image = result.get("image")
		output_str += " > __{}__\n  `[`[View]({})`]`  \n\n".format(text, url)
	end = datetime.now()
	ms = (end - start).seconds
	await gs.edit("Searched on Google for `{}` in `{}` seconds. \n\n{}".format(input_str, ms, output_str), link_preview=False)
	await asyncio.sleep(3)
	await gs.edit("**Google Search:** `{}`\n\n{}".format(input_str, output_str), link_preview=False)

@register(outgoing=True, pattern='^.gi(?: |$)(.*)')
async def gimages(gi):
	if gi.fwd_from:
		return
	execute = True
	try:
		keker = gi.text.split()
		kekar = keker[1]
		if kekar == 'nsfw':
			execute = False
	except Exception:
		pass
	if not execute:
		return
	start = datetime.now()
	upto = random.randint(20, 30)
	await gi.edit(f"`🔎 Searching for images...` \n(This may take upto {upto} secs.)")
	input_str = gi.pattern_match.group(1)
	response = google_images_download.googleimagesdownload()
	arguments = {
		"keywords": input_str,
		"limit": 9,
		"format": "jpg",
		"delay": 1,
		"safe_search": True,
		"output_directory": TEMP_DOWNLOAD_DIRECTORY
	}
	paths = response.download(arguments)
	LOGS.info(paths)
	lst = paths[0].get(input_str)
	await gi.client.send_file(
		gi.chat_id,
		lst,
		caption=input_str,
		reply_to=gi.message.id,
		progress_callback=progress
	)
	LOGS.info(lst)
	for each_file in lst:
		os.remove(each_file)
	end = datetime.now()
	ms = (end - start).seconds
	await gi.edit("Searched on Google Images for `{}` in `{}` seconds.".format(input_str, ms), link_preview=False)
	await asyncio.sleep(3)
	await gi.delete()

@register(outgoing=True, pattern='^.gi nsfw(?: |$)(.*)')
async def gnimages(gin):
	if gin.fwd_from:
		return
	start = datetime.now()
	upto = random.randint(20, 30)
	await gin.edit(f"`🔎 Searching for images w/o safe search...` \n(This may take upto {upto} secs.)")
	input_str = gin.pattern_match.group(1)
	response = google_images_download.googleimagesdownload()
	arguments = {
		"keywords": input_str,
		"limit": 9,
		"format": "jpg",
		"delay": 1,
		"safe_search": False,
		"output_directory": TEMP_DOWNLOAD_DIRECTORY
	}
	paths = response.download(arguments)
	LOGS.info(paths)
	lst = paths[0].get(input_str)
	await gin.client.send_file(
		gin.chat_id,
		lst,
		caption=input_str,
		reply_to=gin.message.id,
		progress_callback=progress
	)
	LOGS.info(lst)
	for each_file in lst:
		os.remove(each_file)
	end = datetime.now()
	ms = (end - start).seconds
	await gin.edit("Searched on Google Images w/o Safe Search for `{}` in `{}` seconds.".format(input_str, ms), link_preview=False)
	await asyncio.sleep(3)
	await gin.delete()

@register(outgoing=True, pattern='^.grs(?: |$)(.*)')
async def grsearch(grs):  
	if grs.fwd_from:
		return
	start = datetime.now()
	BASE_URL = "http://www.google.com"
	OUTPUT_STR = "Reply to an image to do Google Reverse Search!"
	if grs.reply_to_msg_id:
		await grs.edit("`⌛️ Pre-processing media ...`")
		previous_message = await grs.get_reply_message()
		previous_message_text = previous_message.message
		if previous_message.media:
			downloaded_file_name = await grs.client.download_media(
				previous_message,
				TEMP_DOWNLOAD_DIRECTORY
			)
			SEARCH_URL = "{}/searchbyimage/upload".format(BASE_URL)
			multipart = {
				"encoded_image": (downloaded_file_name, open(downloaded_file_name, "rb")),
				"image_content": ""
			}
			# https://stackoverflow.com/a/28792943/4723940
			google_rs_response = requests.post(SEARCH_URL, files=multipart, allow_redirects=False)
			the_location = google_rs_response.headers.get("Location")
			os.remove(downloaded_file_name)
		else:
			previous_message_text = previous_message.message
			SEARCH_URL = "{}/searchbyimage?image_url={}"
			request_url = SEARCH_URL.format(BASE_URL, previous_message_text)
			google_rs_response = requests.get(request_url, allow_redirects=False)
			the_location = google_rs_response.headers.get("Location")
		await grs.edit("Found Google Result. Pouring some soup on it!")
		headers = {
			"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0"
		}
		response = requests.get(the_location, headers=headers)
		soup = BeautifulSoup(response.text, "html.parser")
		# document.getElementsByClassName("r5a77d"): PRS
		prs_div = soup.find_all("div", {"class": "r5a77d"})[0]
		prs_anchor_element = prs_div.find("a")
		prs_url = BASE_URL + prs_anchor_element.get("href")
		prs_text = prs_anchor_element.text
		# document.getElementById("jHnbRc")
		img_size_div = soup.find(id="jHnbRc")
		img_size = img_size_div.find_all("div")
		end = datetime.now()
		ms = (end - start).seconds
		OUTPUT_STR = """ {img_size}\n\nPossible Related Search: <a href="{prs_url}">{prs_text}</a>

More Info: Open <a href="{the_location}">this</a> link """.format(**locals())
	await grs.edit(OUTPUT_STR, parse_mode="HTML", link_preview=False)

@register(outgoing=True, pattern="^.rbg(?: |$)(.*)")
async def kbg(remob):
	""" For .rbg command, Remove Image Background. """
	if not remob.text[0].isalpha() and remob.text[0] not in ("/", "#", "@",
															 "!"):
		if REM_BG_API_KEY is None:
			await remob.edit(
				"`Error: Remove.BG API key missing! Add it to environment vars or config.env.`"
			)
			return
		input_str = remob.pattern_match.group(1)
		message_id = remob.message.id
		if remob.reply_to_msg_id:
			message_id = remob.reply_to_msg_id
			reply_message = await remob.get_reply_message()
			await remob.edit("`Processing..`")
			try:
				if isinstance(
						reply_message.media, MessageMediaPhoto
				) or "image" in reply_message.media.document.mime_type.split(
						'/'):
					downloaded_file_name = await remob.client.download_media(
						reply_message, TEMP_DOWNLOAD_DIRECTORY)
					await remob.edit("`Removing background from this image..`")
					output_file_name = await ReTrieveFile(downloaded_file_name)
					os.remove(downloaded_file_name)
				else:
					await remob.edit(
						"`How do I remove the background from this ?`")
			except Exception as e:
				await remob.edit(str(e))
				return
		elif input_str:
			await remob.edit(
				f"`Removing background from online image hosted at`\n{input_str}"
			)
			output_file_name = await ReTrieveURL(input_str)
		else:
			await remob.edit(
				"`I need something to remove the background from.`")
			return
		contentType = output_file_name.headers.get("content-type")
		if "image" in contentType:
			with io.BytesIO(output_file_name.content) as remove_bg_image:
				remove_bg_image.name = "removed_bg.png"
				await remob.client.send_file(
					remob.chat_id,
					remove_bg_image,
					caption="Background removed using remove.bg",
					force_document=True,
					reply_to=message_id)
				await remob.delete()
		else:
			await remob.edit(
				"**Error (Invalid API key, I guess ?)**\n`{}`".format(
					output_file_name.content.decode("UTF-8")))


# this method will call the API, and return in the appropriate format
# with the name provided.
async def ReTrieveFile(input_file_name):
	headers = {
		"X-API-Key": REM_BG_API_KEY,
	}
	files = {
		"image_file": (input_file_name, open(input_file_name, "rb")),
	}
	r = requests.post("https://api.remove.bg/v1.0/removebg",
					  headers=headers,
					  files=files,
					  allow_redirects=True,
					  stream=True)
	return r


async def ReTrieveURL(input_url):
	headers = {
		"X-API-Key": REM_BG_API_KEY,
	}
	data = {"image_url": input_url}
	r = requests.post("https://api.remove.bg/v1.0/removebg",
					  headers=headers,
					  data=data,
					  allow_redirects=True,
					  stream=True)
	return r


CMD_HELP.update({
	"rbg":
	".rbg <Link to Image> or reply to any image (does not work on stickers.)\
\nUsage: Removes the background of images, using remove.bg API"
})


CMD_HELP.update({
	"figlet":
	"Usage: Provide a font(random for randomness) and text to render it with Figlet.\n"
	"Syntax: `.figlet <Font> <Text>`"
})


CMD_HELP.update({
	"gifify":
	"Usage: reply to animated sticker to convert it into GIF\n"
	"Syntax: `.gifify <Reply>`"
})

CMD_HELP.update({
	"lyrics":
	"Usage: provide artist and song name to find lyrics\n"
	"For multiple-word artist name use * (Exmpl: .`lyrics Валентин*Стрыкало Все решено`)"
})

CMD_HELP.update({
	'barcode':
	".barcode <content>\n"
	"Usage: Make a Barcode from the given content.\n"
	"Example: .barcode 777"
})

CMD_HELP.update({
	"coinflip":
	"Flip a coin!"
})

CMD_HELP.update({
	"gs":
	".gs <query> - Search in google"
})

CMD_HELP.update({
	"gi":
	".gi <query> - Search on google images\n.gi nsfw <query> - Search in google images w/o safe search"
})

CMD_HELP.update({
	"grs":
	".grs <reply to photo> - Search in google images by photo"
})