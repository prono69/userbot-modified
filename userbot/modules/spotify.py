# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.c (the "License");
# you may not use this file except in compliance with the License.
#
from asyncio import sleep
from json import loads
from json.decoder import JSONDecodeError
from os import environ
from sys import setrecursionlimit

import spotify_token as st
from requests import get
from telethon.errors import AboutTooLongError
from telethon.tl.functions.account import UpdateProfileRequest
from telethon.tl.functions.users import GetFullUserRequest

from userbot import (BIO_PREFIX, BOTLOG, BOTLOG_CHATID, CMD_HELP, DEFAULT_BIO,
                     SPOTIFY_PASS, SPOTIFY_USERNAME, bot, is_mongo_alive, MONGO)
from userbot.events import register

# =================== CONSTANT ===================
SPO_BIO_ENABLED = "`Spotify current music to bio is now enabled.`"
SPO_BIO_DISABLED = "`Spotify current music to bio is now disabled. "
SPO_BIO_DISABLED += "Bio will be back right now.`"
SPO_BIO_RUNNING = "`Spotify current music to bio is already running.`"
ERROR_MSG = "`Spotify module halted, got an unexpected error.`"
NO_SQL = "`*Wroom wroom* Database connection error!`"

USERNAME = SPOTIFY_USERNAME
PASSWORD = SPOTIFY_PASS

ARTIST = 0
SONG = 0

BIOPREFIX = BIO_PREFIX

SPOTIFYCHECK = False
RUNNING = False
OLDEXCEPT = False
PARSE = False


# ================================================
async def get_spotify_token():
    sptoken = st.start_session(USERNAME, PASSWORD)
    access_token = sptoken[0]
    environ["spftoken"] = access_token

async def writebio(bio):
    if not is_mongo_alive:
        raise SQLError(NO_SQL)
    else:
        to_check = MONGO.spoty.find_one({ 'has': True })
        if not to_check:
            MONGO.spoty.insert_one({ 'bio': bio, 'has': True })
        else:
            MONGO.spoty.update_one({ '_id': to_check['_id'], 'has': True, 'bio': to_check['bio'] },
                                    {'$set': {
                                        'bio': bio
                                    }})
        return 'OK'
async def getbio():
    if not is_mongo_alive:
        raise SQLError(NO_SQL)
    else:
        to_check = MONGO.spoty.find_one({ 'has': True })
        if not to_check:
            raise BIOError('Bio not found!')
        else:
            return to_check['bio']
async def rembio():
    if not is_mongo_alive:
        raise SQLError(NO_SQL)
    else:
        to_check = MONGO.spoty.find_one({ 'has': True })
        if not to_check:
            raise BIOError('Bio not found!')
        else:
            MONGO.spoty.delete_one({
                '_id': to_check['_id'],
                'bio': to_check['bio'],
                'has': to_check['has']
            })
            return 'OK'


async def update_spotify_info():
    global ARTIST
    global SONG
    global PARSE
    global SPOTIFYCHECK
    global RUNNING
    global OLDEXCEPT
    oldartist = ""
    oldsong = ""
    while SPOTIFYCHECK:
        try:
            RUNNING = True
            spftoken = environ.get("spftoken", None)
            hed = {'Authorization': 'Bearer ' + spftoken}
            url = 'https://api.spotify.com/v1/me/player/currently-playing'
            response = get(url, headers=hed)
            data = loads(response.content)
            artist = data['item']['album']['artists'][0]['name']
            song = data['item']['name']
            OLDEXCEPT = False
            oldsong = environ.get("oldsong", None)
            if song != oldsong and artist != oldartist:
                oldartist = artist
                environ["oldsong"] = song
                spobio = BIOPREFIX + " 🎧: " + artist + " - " + song
                try:
                    await bot(UpdateProfileRequest(about=spobio))
                except AboutTooLongError:
                    short_bio = "🎧: " + song
                    await bot(UpdateProfileRequest(about=short_bio))
                environ["errorcheck"] = "0"
        except KeyError:
            errorcheck = environ.get("errorcheck", None)
            if errorcheck == 0:
                await update_token()
            elif errorcheck == 1:
                SPOTIFYCHECK = False
                await bot(UpdateProfileRequest(about=DEFAULT_BIO))
                print(ERROR_MSG)
                if BOTLOG:
                    await bot.send_message(BOTLOG_CHATID, ERROR_MSG)
        except JSONDecodeError:
            OLDEXCEPT = True
            await sleep(6)
            await bot(UpdateProfileRequest(about=DEFAULT_BIO))
        except TypeError:
            await dirtyfix()
        SPOTIFYCHECK = False
        await sleep(2)
        await dirtyfix()
    RUNNING = False


async def update_token():
    sptoken = st.start_session(USERNAME, PASSWORD)
    access_token = sptoken[0]
    environ["spftoken"] = access_token
    environ["errorcheck"] = "1"
    await update_spotify_info()


async def dirtyfix():
    global SPOTIFYCHECK
    SPOTIFYCHECK = True
    await sleep(4)
    await update_spotify_info()


@register(outgoing=True, pattern="^.enablespotify$")
async def set_biostgraph(setstbio):
    setrecursionlimit(700000)
    if not SPOTIFYCHECK:
        environ["errorcheck"] = "0"
        await setstbio.edit(SPO_BIO_ENABLED)
        bio = (await bot(GetFullUserRequest((await bot.get_me()).id))).about
        if not bio:
            bio = DEFAULT_BIO
        await writebio(bio)
        await get_spotify_token()
        await dirtyfix()
    else:
        await setstbio.edit(SPO_BIO_RUNNING)


@register(outgoing=True, pattern="^.disablespotify$")
async def set_biodgraph(setdbio):
    global SPOTIFYCHECK
    global RUNNING
    SPOTIFYCHECK = False
    RUNNING = False
    try:
        bio = await getbio()
    except Exception as e:
        return await setdbio.edit(str(e))
    try:
        await rembio()
    except Exception as e:
        return await setdbio.edit(str(e))
    await bot(UpdateProfileRequest(about=bio))
    await setdbio.edit(SPO_BIO_DISABLED)


CMD_HELP.update({"enablespotify": "Usage: Enable Spotify bio updating."})

CMD_HELP.update({"disablespotify": "Usage: Disable Spotify bio updating."})
